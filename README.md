#This website is a mobile-friendly clone of Microsoft's Landing Page without any JavaScript. This was built to showcase my ability and extensive knowledge to HTML5 and CSS3.

##See Live Demo --> https://tomislavkraljic.github.io/static-layout/

<img src="Screen Shot 2020-08-02 at 5.26.55 PM.png" height="500" width="800">
